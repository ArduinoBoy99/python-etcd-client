# Python Etcd Client

Simple command line for getting and setting single etcd key/value.
Starts from the command line, requeried arguments are 
host, port, user and password.

Optional parameters are ca_cert, cert_key, cert_cert, timeout and grcp_options.

After starting and forming etcd client from povided parameters the program
displays main menu with 3 options:

1. Set
2. Get
3. Exit


Option set will ask for key/value input to be set,
option get will ask for a key which will be used to find value and print it,
option exit will exit the program.

On startup error, if client can't be configured, the program will shutdown
with an error message.
On any input error, or etcd error while trying to set/get, program will
return user back to main menu.

# Example usage
$ python3 etcd-client-main.py "127.0.0.1" 80 userName passWord
