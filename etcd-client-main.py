import etcd3
import argparse
import sys

MAIN_MENU = '''  
1 Set
2 Get
3 Exit
'''


def set_etcd_config(host="localhost", port=2379, ca_cert="", cert_key="", cert_cert="",
                    timeout="", user="", password="", grcp_options=""):
    etcd_config_dict = {
        "host": host,
        "port": port,
        "ca_cert": ca_cert,
        "cert_key": cert_key,
        "cert_cert": cert_cert,
        "timeout": timeout,
        "user": user,
        "password": password,
        "grcp_options": grcp_options,
    }
    return etcd_config_dict


def create_etcd_client(config):
    etcd_client = etcd3.Etcd3Client(config.get("host"), config.get("port"))
    return etcd_client


def get_etcd_value(etcd, key):
    value = etcd.get(key)
    return value


def put_etcd_value(etcd, key, value, lease=None, prev_kv=False):
    etcd.put(key, value, lease, prev_kv)


def replace_etcd_value(etcd, key, initial_value, new_value):
    return etcd.replace(key, initial_value, new_value)


def putValueUser(etcdClient):
    value = input("Enter value to put: ")
    key = input("Enter key to put: ")
    try:
        put_etcd_value(etcdClient, key, value)
    except Exception:
        print("Error while parsing specified etcd key/values!")
        return
    else:
        print("Value successfully set to etcd.")

def getValueUser(etcdClient):
    key = input("Enter key to read: ")
    try:
        value = get_etcd_value(etcdClient, key)
    except Exception:
        print("Error while parsing specified etcd key!")
        return
    else:
        print(value)


def main():
    print("Starting etcd client...")

    # Create the parser
    parser = argparse.ArgumentParser(prog='Py-etcd-client', description='Read or put etcd values..')

    # Add arguments
    parser.add_argument('host', metavar='host', type=str, help='Etcd3 host')
    parser.add_argument('port', metavar='port', type=int, help='Etcd3 Port')
    parser.add_argument('--ca_cert', metavar='ca_cert', type=str, help='Etcd3 Ca_cert path')
    parser.add_argument('--cert_key', metavar='cert_key', type=str, help='Etcd3 Cert_key path')
    parser.add_argument('--cert_cert', metavar='cert_cert', type=str, help='Etcd3 Cert_cert path')
    parser.add_argument('--timeout', metavar='timeout', type=int, help='Etcd3 connection timeout')
    parser.add_argument('User', metavar='user', type=str, help='Etcd3 username')
    parser.add_argument('Password', metavar='password', type=str, help='Etcd3 password')
    parser.add_argument('--grcp_options', metavar='grcp_options', type=str, help='Etcd3 grcp_options')

    # Execute the parse_args() method
    args = parser.parse_args()

    host = args.host
    port = args.port
    ca_cert = args.ca_cert
    cert_key = args.cert_key
    cert_cert = args.cert_cert
    timeout = args.timeout
    user = args.User
    password = args.Password
    grcp_options = args.grcp_options

    try:
        etcdConfig = set_etcd_config(host, port, ca_cert, cert_key, cert_cert, timeout, user, password, grcp_options)
    except Exception:
        print("Error while setting etcd config from input!")
        print("Shutting program down..")
        sys.exit(1)

    print("EtcdConfig:", etcdConfig)
    try:
        etcdClient = create_etcd_client(etcdConfig)
    except Exception:
        print("Error while creating etcd client!")
        print("Shutting program down..")
        sys.exit(1)

    while True:
        print(MAIN_MENU)

        exception = True
        while exception:
            entryCode = input()
            try:
                entryCodeInt = int(entryCode)
            except Exception:
                print("Wrong type of input, enter a number!\n")
            else:
                exception = False

        if entryCodeInt == 1:
            putValueUser(etcdClient)
        elif entryCodeInt == 2:
            getValueUser(etcdClient)
        elif entryCodeInt == 3:
            exit()
        else:
            print("Enter a number listed in options!\n")


if __name__ == "__main__":
    main()
